extern crate chrono;
use chrono::{UTC, TimeZone};

fn process_line(line: String) -> Option<String> {
		let mut columns: Vec<&str> = line.split(',').collect();
		let timestamp = columns[0].parse::<i64>();
		if timestamp.is_err() {
			return None;
		}
		let datatime = UTC.timestamp(timestamp.unwrap(), 0).to_string();
		columns[0] = datatime.as_str();
		Some(columns.join(","))
	}

fn main() {
	// Convert the timestamp in first column to datetime string
	let s = String::from("1489544029,20");
	process_line(s).unwrap();
}
